modelString = "
  model {  

    #------------------------------------------------------------
    # ... : 
    for(t in 1:length(y))
    {
      z[t] <- beta[1] * X1[t] + beta[2] * X2[t]
      p[t] <- 1 / (1 + exp(-z[t]))
      y[t] ~ dbern(p[t])
    }
    
    #------------------------------------------------------------
    # Prior: 

    for(i in 1:2)
    {
      beta[i] ~ dbeta(a, b)
    }

    #------------------------------------------------------------
    # Konstanten

    #------------------------------------------------------------
    # Hyperprior
    gruppenIndex ~ dcat(gruppeProb[])
    gruppenProb[1] <-   alpha
    gruppenProb[2] <- 1-alpha

    alpha <- 0.9
  
  }
"
model <- jags.model(textConnection(modelString),
                    data = data,
                    n.chains = 3,
                    n.adapt = 1e3)

update(model, 1000)
samples <- coda.samples(model, variable.names=c("gruppe.a", "gruppe.b"), n.iter=1000)
summary(samples)
# par(mar = rep(2, 4))
plot(samples)

data <- list(     y = c(storedata[[1]]$zielVariable, storedata[[2]]$zielVariable)
                ,X1 = c(storedata[[1]]$sessionLength, storedata[[2]]$sessionLength)
                ,X2 = c(storedata[[1]]$prevConversion, storedata[[2]]$prevConversion))


coef(lm('y ~ .', data))

