##############
#### Example Code
#### Florian Nottorf (2013)
##############

rm(list = ls(all = TRUE))
set.seed(1)

##############
#### import data
##############
load("./test.RData")

##############
#### convert time
##############
colnames(data) = c("userID","Y","time","channel")
data$time <- strptime(data$time, format="%Y-%m-%d %H:%M:%S")

##############
#### order data
##############
data<-data[order(data$userID,data$time),]

##############
#### prepare dataset 
##############
userID <- matrix(data$userID)
time <- data$time
channel <- matrix(data$channel)
Y <- matrix(data$Y)

b_data <- NULL
count <- 0
currentID <- userID[1]
erg_time <- time[1]
erg_channel <- channel[1]
erg_y <- Y[1]
for (j in 2:nrow(data)){
  if (currentID==userID[j]){
    erg_time <- c(erg_time,time[j])
    erg_channel <- c(erg_channel, channel[j])
    erg_y <- c(erg_y,Y[j])
  }else{
    count <- count+1
    b_data[[count]] <- list(time=erg_time, channel=erg_channel,y=erg_y)
    currentID <- userID[j]
    erg_time <- time[j]
    erg_channel <- channel[j]
    erg_y <- Y[j]
  }
}

##############
#### specify channels
##############
Xchannel <- list(
  affiliate = c("AFFILIATE"), 
  preissuchmaschine = c("PREISSUCHMASCHINE"),
  referrer = c("REFERRER"),
  retargeting = c("RETARGETING"),
  search = c("SEA","SEA_GENERIC","SEO"),
  newsletter =  c("NEWSLETTER"),
  other= c("IA_TARGETING","E-IA_TARGETING","SOCIAL","TYPE_IN")
)

##############
#### build dataset 
##############
preparedata <- function(b_data,Xchannel){
  erg <- NULL
  storedata = NULL
  for (i in 1:length(b_data)){
    time <- b_data[[i]]$time
    erg <- NULL
    store <- NULL
    y = NULL
    count <- 1
    
    ##############
    #### determine y
    ##############
    if(b_data[[i]]$y[1]=="CLICK"){
      y <- 1
      
      ##############
      #### determine x
      ##############
      for(k in 1:length(Xchannel)){
        if(is.element(b_data[[i]]$channel[1],Xchannel[[k]])){
          erg <- c(erg,1)
        }else{
          erg <- c(erg,0)
        }
      }
      store <- rbind(store,erg)
      
    }else{
      time <- time[-1]
      count <- 0
    }
    
    if(length(b_data[[i]]$y)>1){
      for (j in 2:length(b_data[[i]]$y)){
        erg <- NULL
        ##############
        #### determine y
        ##############
        if(b_data[[i]]$y[j]=="CLICK"){
          count <- count + 1
          y <- c(y,1)
          ##############
          #### determine x
          ##############
          for(k in 1:length(Xchannel)){
            if(is.element(b_data[[i]]$channel[j],Xchannel[[k]])){
              erg <- c(erg,1)
            }else{
              erg <- c(erg,0)
            }
          }
          store <- rbind(store,erg)
        }else{
          if(count!=0){
            y[(count)] <- 2
          }
          time <-time[-j]
        }
      }
    }
    storedata[[i]] <- list(y=y,X=store,time=time)
  }
  storedata
}

storedata <- preparedata(b_data,Xchannel)
storedata[[1]]
b_data[[1]]
storedata[[9]]
##############
### load packages
##############
library(bayesm)

##############
### set first parameters
##############
p=2 # number of choice alternatives

##############
### prepare data for analysis
##############
preplgtdata <- function(lgtdata, obs){
  count<-0
  dataoutput <- NULL
  for(i in 1:length(lgtdata)){
    journeylength <- length(lgtdata[[i]]$y)
    if(journeylength>8){ ## be sure to have only user with at least x queries
      if (i<100) cat(paste(i,count," --- "))
      count <- count + 1
      X=NULL
      y <- lgtdata[[i]]$y[1]
      
      ### model intrasession (X) ad-covariates
      Xtemp <- cbind(lgtdata[[i]]$X)
      Xd <- matrix(c(Xtemp[1,]),nrow=1)
      Xconv <- 0 
      
      ### model intersession (Y) ad-covariates
      Yd <- matrix(rep(0,length(Xd)),nrow=1)
      Yconv <- 0
      
      ### model additional variables
      IST <- 0 ## intersession time
      session <- 1 ## first session
      priorconv <- 0 ## prior conversion
      advar = cbind(priorconv,IST,session)
      
      ###createX
      X=rbind(X,createX(p,na=NULL,nd=length(cbind(Xd,Xconv,Yd,Yconv,advar)),
                        Xa=NULL,Xd=cbind(Xd,Xconv,Yd,Yconv,advar),base=1,INT=T))
      
      for(j in 2:journeylength){
        y <- c(y,lgtdata[[i]]$y[j])
        priorconv <- ifelse(y[j-1]==2,1,0) ## prior conv
        
        ### check if new session
        lastsession <- session[j-1]
        session <- c(session,session[j-1]+
                       ifelse(difftime(lgtdata[[i]]$time[j],lgtdata[[i]]$time[j-1],units="mins")<60,0,1))
        
        ### model session specific covariates
        if(lastsession==session[j]){
          Xd <- Xd + matrix(c(Xtemp[j,]),nrow=1)
          Xconv <- Xconv + priorconv
          
          ### intersession time = 0
          IST <- c(IST, IST[j-1])
        }else{
          ###model intersession (Y) ad-covariates
          Yd <- Yd + Xd
          Yconv <- Yconv + Xconv
          
          Xconv <- priorconv
          Xd <- matrix(c(Xtemp[j,]),nrow=1)
          
          ### calculate intersession time
          IST <- c(IST, log(as.numeric(difftime(lgtdata[[i]]$time[j],lgtdata[[i]]$time[j-1],units="hours"))+1))
        }
        
        ###model additional variables (Ad)
        advar = cbind(priorconv,IST[j],session[j])
        
        ###createX
        X=rbind(X,createX(p,na=NULL,nd=length(cbind(Xd,Xconv,Yd,Yconv,advar)),
                          Xa=NULL,Xd=cbind(Xd,Xconv,Yd,Yconv,advar),base=1,INT=T))
      }
      dataoutput[[count]] <- list(X=X, y=y)
    }
  }
  dataoutput
}

##############
### prepare data for analysis
##############
lgtdata <- preplgtdata(storedata)

##############
### set parameters
##############
burn=100 ### number of burn in iterations
R=200    ### number of MCMC iterations
keep=1   ### every xth iteration is stored
ncomp=2   ### number of mixture components

##############
### set parms for priors
##############
Prior=list(ncomp=ncomp)
Mcmc=list(R=R,keep=keep)
Data=list(p=p,lgtdata=lgtdata)

##############
### start MCMC analysis
##############
out=rhierMnlRwMixture(Data=Data,Prior=Prior,Mcmc=Mcmc)

##############
### output analysis
##############
coef_names <- c("Intercept",paste("X",names(Xchannel)),"XConv",paste("Y",names(Xchannel)),"YConv","priorconv","IST","Session")

mixture_draws <- NULL
for (numberofelement in 1:ncol(lgtdata[[1]]$X)){
  erg <- NULL
  for (i in 1:ncomp){
    temp <- NULL
    for (j in (burn/keep):(R/keep)){
      temp <- c(temp,out$nmix[[3]][[j]][[i]]$mu[numberofelement])
    }
    erg <- rbind(erg,quantile(temp,c(0.05,.5,.95)))
  }
  mixture_draws[[numberofelement]] <-  list(coef_names[numberofelement],erg)
}
erg