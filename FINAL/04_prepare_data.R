########################################### Datenbereinigung

# Timeformat
data$datetime <- as.POSIXct(data$datetime)

# Order
data <- data[order(data$userkey,data$datetime),]

## 
# Prepare Dataset
#
# Prüfe ob der Wert davor den gleichen Userkey besitzt. Ist dies der Fall wird aus der Zeile
# die $datetime verwendet. Andernfalls wird NA gesetzt. 
##
mx_user     <- matrix(data$userkey)
df_time     <- data$datetime
mx_type     <- matrix(data$type)
mx_publ     <- matrix(data$publisher)
mx_wday     <- matrix(data$weekday)

b_data        <- NULL                           #
count         <- 0                              # 
cur_user      <- mx_user[1]                     # Current Userkey
erg_time      <- df_time[1]
erg_type      <- mx_type[1]
erg_publ      <- mx_publ[1]
erg_wday      <- mx_wday[1]
for( i in 2:nrow(data) ) 
{
  if( cur_user == mx_user[i] )  
  { # Wenn der aktuelle User mit dem davor gleich ist.
    erg_time <- c(erg_time, df_time[i])
    erg_type <- c(erg_type, mx_type[i])
    erg_publ <- c(erg_publ, mx_publ[i])
    erg_wday <- c(erg_wday, mx_wday[i])
  }
  else 
  { # Wenn der aktuelle User nicht der gleiche ist, wie der davor.
    count           <- count + 1
    b_data[[count]] <- list(time=erg_time, type=erg_type, publisher=erg_publ, weekday=erg_wday)
    cur_user        <- mx_user[i] 
    erg_time        <- df_time[i]
    erg_type        <- mx_type[i]
    erg_publ        <- mx_publ[i]
    erg_wday        <- mx_wday[i]
  }
}

## 
# Specify Publisher
#
# SELECT DISTINCT publisher
#   FROM lfp_electronic; 
#####
xref_publ <- list(
  rtb                = c('AdAudience', 'Affilinet', 'd3media', 'Vertical Techmedia'),
  search_other       = c('Ask', 'Baidu', 'Bing.com', 'Blekko', 'DuckDuckGo', 'Fireball', 'WEB.DE', 'Yahoo', 'Yahoo DE'),
  search_google      = c('Google Adwords', 'Google DE', 'Google Display Network', 'Google UK', 'Google US'),
  newsletter         = c('Newsletter', 'Reaktivierungsmail'),
  website            = c('com-magazin', 'Computerbild', 'Ebay', 'Electronic Weblessons', 'Elektor', 'elektroniknet', 'Elektronikpraxis', 'Hardware Hacks', 'Heise', 'hukd.mydealz.de', 'SupplyFrame', 'www.mydealz.de'),
  preissuchmaschine  = c('billiger.de', 'chip-preisvergleich', 'ciao.de', 'evendi.de', 'gÃ¼nstiger.de', 'geizhals.de', 'geizkragen.de', 'hardwareschotte.de', 'idealo.de', 'Mistershoplister', 'preis.de', 'preisroboter.de', 'preistrend.de', 'shopping.com'),
  social             = c('Facebook'),
  other              = c('Bellnet', 'Commercecenter', 'Roboter Netz', 'Sharelook', '')
)

##
# Specify Types
#
# [description]
##
xref_types <- names(summary(data$type))

#####
# prepareData(b_data, xref_publ, xref_types)
#
# [description]
#
# @param    b_data        list      [description]
# @param    xref_publ     vector    [description]
# @param    xref_types    vector    [description]
# 
# @return                 list      [description]
#####
funcPrepareData <- function( b_data, xref_publ, xref_types ) {
  # Variablen initialsieren
  erg       <- NULL
  storedata <- NULL
  result    <- list()
  count     <- 0
  
  # 1. Schleife die jeden User durchgeht
  for( i in 1:length(b_data) )                         # Wird aktuell 1:9 durchgeganngen
  {
    # Variablen Reset für jeden User
    erg   <- NULL
    store <- NULL
    count <- 1
    time  <- b_data[[i]]$time
    type  <- NULL
    publ  <- NULL
    wday  <- b_data[[i]]$weekday
    
    # Jeden Touchpoint durchgehen
    for( t in 1:length(b_data[[i]]$type) )            # Wird aktuell 1:11 durchgeganngen 
    { 
      # Type wird anhand des Index Wertes von xref_types gesetzt
      type_hilf <- which(xref_types==b_data[[i]]$type[t])
      type <- c(type, type_hilf)
      erg = NULL
      # Publisher X Types Matrix erstellen
      
      for( p in 1:length(xref_publ) )                 # Wird aktuell 1:8 durchgeganngen 
      {
        # if( is.element(b_data[[i]]$publ[1], xref_publ[[p]]) )
        if( is.element(b_data[[i]]$publ[t], xref_publ[[p]]) )
        {
          erg <- c(erg,1)
        }
        else 
        {
          erg <- c(erg,0)
        }
      } # /end for(Publisher)
      # Ausgabe für den einzelnen Y-Wert
      store <- rbind(store, erg)
    } # /end for(Touchpoint)
    
    # Variablen berechnen
    result.lastActionTime       <- funcLastActionTime( b_data[[i]] )
    result.touchpointLength     <- funcTouchpointLength( b_data[[i]] )
    result.sessionIdentifier    <- funcSessionIdentifier( b_data[[i]] )
    result.sessionLength        <- funcSessionLength( b_data[[i]] )
    result.timeOnSiteInSession  <- funcTimeOnSiteInSession( b_data[[i]], result.sessionLength )
    result.timeOfDayCluster     <- funcTimeOfDayCluster( b_data[[i]] )
    result.prevConversion       <- funcPrevConversion( b_data[[i]] )
    result.avgSessionTime       <- funcAvgSessionTime( b_data[[i]] )
    result.zielVariable         <- funcZielVariable( b_data[[i]] )
    result.prevConvTimeDiff     <- funcPrevConvTimeDiff( b_data[[i]] )
    
    # Ausgabe pro User
    storedata[[i]] <- list(  publisherXtype=store
                             ,weekday=wday
                             ,type=type
                             ,time=time
                             ,lastActionTime=result.lastActionTime
                             ,touchpointLength=result.touchpointLength
                             ,sessionIdentifier=result.sessionIdentifier
                             ,sessionLength=result.sessionLength
                             ,timeOnSiteInSession=result.timeOnSiteInSession
                             ,timeOfDayCluster=result.timeOfDayCluster
                             ,prevConversion=result.prevConversion
                             ,avgSessionTime=result.avgSessionTime
                             ,zielVariable=result.zielVariable
                             ,prevConvTimeDiff=result.prevConvTimeDiff)
  } # /end for(User)
  
  return(storedata)
}