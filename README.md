# Topic
Trotz umfangreicher Datenbestände, neuer Analysemethoden und einer Vielzahl von Datentypen im Bereich der Online Werbung steckt die kanal- und ebenenübergreifende Werbewirkungsforschung noch in den Kinderschuhen. Die steigende Anzahl an wissenschaftlichen Publikationen und das gleichzeitige Interesse von Unternehmen an den Forschungsergebnissen weisen aber auf ein attraktives, sich schnell entwickelndes und hochgradig praxisrelevantes Forschungsfeld hin.

Im Rahmen dieses Lehrforschungsprojektes setzen Sie sich mit einer konkreten empirischen Forschungsfrage auseinander (die empirischen Daten werden von unseren Praxispartnern zur Verfügung gestellt). Ziel ist es, dass jedes Team in der Veranstaltung ein (erstes) wissenschaftliches Paper erarbeitet und möglichst bei einem Journal oder einer einschlägigen wissenschaftlichen Konferenz einreicht. Sowohl beim Erlernen der für die Forschungsfrage geeigneten Methode, bei der Analyse der empirischen Daten als auch bei der Erarbeitung des Papers werden wir Sie intensiv unterstützen.

# Goal
Selbständige, tiefgehende Auseinandersetzung mit modellbildenden Ansätzen (Datenverständnis und - analyse, Modellbildung, Modellvalidierung mit Hilfe der Datensimulation, State-of-the-art Methoden)
Studierende lernen anhand einer konkreten Forschungsfrage, empirische Daten gezielt zu analysieren und für die Modellbildung heranzuziehen.

# Contribute
Benedict Ernst @ernstzunehmend